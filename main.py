import pygame
from pygame.locals import *
from random import randint
from icecream import ic
import sys, time, random

# 17:00 ep. 3-Boss - just added boss sprites and group

# Colors
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (103, 150, 86)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)

WIDTH = 600
HEIGHT = 800

def sprite_sheet(size, file, pos=(0, 0)):
    """ cuts spritesheets into individual sprites one row at a time from left to right and return them as a list of lists """
    # initial values
    len_sprite_x, len_sprite_y = size # sprite size
    sprite_rect_x, sprite_rect_y = pos # where to find first sprite on sheet
    sheet = pygame.image.load(file).convert_alpha() # load the sheet file
    sheet_rect = sheet.get_rect()
    horizontal_sprites = int(sheet_rect.width / size[0])
    vertical_sprites = int(sheet_rect.height / size[1])
    all_sprites = []
    for row in range(0, vertical_sprites): # rows
        row_sprites = []
        for col in range(0, horizontal_sprites): # columns
            sheet.set_clip(pygame.Rect(sprite_rect_x, sprite_rect_y, len_sprite_x, len_sprite_y)) # cut out the sprite
            single_sprite = sheet.subsurface(sheet.get_clip()) # grab the sprite
            row_sprites.append(single_sprite)
            sprite_rect_x += len_sprite_x

        sprite_rect_y += len_sprite_y
        sprite_rect_x = 0
        all_sprites.append(row_sprites)
    return all_sprites

# create classes
class Player(pygame.sprite.Sprite):
    def __init__(self, game, groups) -> None:
        super().__init__(groups)
        self.game = game
        self.speed = 5
        self.lives = 3
        self.max_lives = 10
        # define sprite characteristics
        self.width = 140
        self.height = 120
        self.spriteX = self.game.width * 0.5 - self.width * 0.5 # spriteX and spriteY define the topleft corner
        self.spriteY = self.game.height - self.height
        self.player_images = sprite_sheet((self.width, self.height), './images/player.png')
        self.jets_images = sprite_sheet((self.width, self.height), './images/player_jets.png')
        self.frameXRow = 0
        self.jetsFrameX = 1

        self.image = self.player_images[0][0]
        self.rect = self.image.get_rect()

    def update(self, dt) -> None:
        # horizontal movement
        if pygame.K_LEFT in self.game.keys:
            self.spriteX -= self.speed
            self.jetsFrameX = 0
        elif pygame.K_RIGHT in self.game.keys:
            self.spriteX += self.speed
            self.jetsFrameX = 2
        else:
            self.jetsFrameX = 1
        if pygame.K_1 in self.game.keys:
            self.image = self.player_images[0][1]
        else:
            self.image = self.player_images[0][0]
        # horizontal boundaries
        if self.spriteX < -self.width * 0.5:
            self.spriteX = -self.width * 0.5
        elif self.spriteX > self.game.width - self.width * 0.5:
            self.spriteX = self.game.width - self.width * 0.5

        # draw player
        self.rect = self.image.get_rect()
        self.rect.x = round(self.spriteX)
        self.rect.y = round(self.spriteY)
        # draw jets
        self.game.screen.blit(self.jets_images[0][self.jetsFrameX], (round(self.spriteX), round(self.spriteY)))

    def shoot(self):
        projectile = self.game.get_free_projectile()
        if projectile:
            projectile.create_it(self.spriteX + self.width * 0.5, self.spriteY)

    def restart(self):
        self.spriteX = self.game.width * 0.5 - self.width * 0.5 # spriteX and spriteY define the topleft corner
        self.spriteY = self.game.height - self.height
        self.lives = 3

class Projectile(pygame.sprite.Sprite):
    def __init__(self, groups) -> None:
        super().__init__(groups)
        self.width = 3
        self.height = 40
        self.spriteX = 0
        self.spriteY = -40
        self.speed = 20
        self.free = True

        self.image = pygame.Surface((self.width, self.height))
        self.image.fill('gold')
        self.rect = self.image.get_rect()

    def update(self, dt) -> None:
        if not self.free:
            self.spriteY -= self.speed
            if self.spriteY < 0 - self.height:
                self.reset()

        self.rect.x = round(self.spriteX)
        self.rect.y = round(self.spriteY)

    def create_it(self, x, y):
        self.spriteX = x - self.width * 0.5
        self.spriteY = y
        self.free = False

    def reset(self):
        self.free = True
        self.spriteX = 0
        self.spriteY = -40

class Enemy(pygame.sprite.Sprite):
    def __init__(self, game, positionX, positionY, groups) -> None:
        super().__init__(groups)
        self.game = game
        self.width = self.game.enemy_size
        self.height = self.game.enemy_size
        self.spriteX = 0
        self.spriteY = 0
        self.positionX = positionX
        self.positionY = positionY
        self.marked_for_deletion = False

        self.screen = pygame.display.get_surface()

        # self.image = pygame.Surface((self.width, self.height))
        # self.image.fill(WHITE)
        # self.rect = self.image.get_rect()

    def hit(self, damage):
        self.lives -= damage

    def update(self, x, y) -> None:
        # update will take x and y position of the wave (line 147 and 148)
        self.spriteX = x + self.positionX
        self.spriteY = y + self.positionY
        # pick up the right sprite
        self.image = self.enemy_images[self.frameY][self.frameX]
        self.rect = self.image.get_rect()
        # check for collision enemies - projectiles
        for projectile in self.game.projectiles_pool:
            if not projectile.free and self.game.check_collisions(self, projectile) and self.lives > 0:
                self.hit(1)
                projectile.reset()
        if self.lives < 1:
            self.frameX += 1
            if self.frameX > self.max_frame:
                self.marked_for_deletion = True
                if not self.game.game_over:
                    self.game.score += self.max_lives
        # check collision enemies - player
        if self.game.check_collisions(self, self.game.player) and self.lives > 0:
            self.lives = 0
            self.game.player.lives -= 1
        # game over condition
        if self.spriteY + self.height > self.game.height or self.game.player.lives < 1:
            self.game.game_over = True
        # pygame.draw.rect(self.screen, 'white', (self.spriteX, self.spriteY, self.width,self.height), 3)
        self.rect.x = round(self.spriteX)
        self.rect.y = round(self.spriteY)

class Beetlemorph(Enemy):
    def __init__(self, game, positionX, positionY, groups) -> None:
        super().__init__(game, positionX, positionY, groups)
        self.enemy_images = sprite_sheet((self.width, self.height), './images/beetlemorph.png')
        # print(f'beetlemorph sprites: {len(self.beetlemorph_images)}')
        self.frameX = 0
        self.max_frame = 2
        self.frameY = randint(0, 3)
        self.lives = 1
        self.max_lives = self.lives

        self.image = self.enemy_images[self.frameY][self.frameX]
        self.rect = self.image.get_rect()

class Rhinomorph(Enemy):
    def __init__(self, game, positionX, positionY, groups) -> None:
        super().__init__(game, positionX, positionY, groups)
        self.enemy_images = sprite_sheet((self.width, self.height), './images/rhinomorph.png')
        self.frameX = 0
        self.max_frame = 5
        self.frameY = randint(0, 3)
        self.lives = 4
        self.max_lives = self.lives

        self.image = self.enemy_images[self.frameY][self.frameX]
        self.rect = self.image.get_rect()

    def hit(self, damage):
        self.lives -= damage
        self.frameX = self.max_lives - self.lives

class Boss(pygame.sprite.Sprite):
    def __init__(self, game, groups) -> None:
        super().__init__(groups)
        self.game = game
        self.width = 200
        self.height = 200
        self.spriteX = self.game.width * 0.5 - self.width * 0.5
        self.spriteY = -self.height
        self.speedX = -1 if random.random() < 0.5 else 1
        self.speedY = 0
        self.lives = 10
        self.max_lives = self.lives
        self.marked_for_deletion = False
        self.boss_images = sprite_sheet((self.width, self.height), './images/boss.png')
        self.frameX = 0
        self.frameY = randint(0, 3)
        self.max_frame = 11

    def hit(self, damage):
        self.lives -= damage
        ic(self.lives)
        if self.lives > 1:
            self.frameX = 1

    def update(self, dt) -> None:
        self.speedY = 0
        if self.game.sprite_update and self.lives > 0:
            ic(self.game.sprite_update)
            self.frameX = 0
        if self.spriteY < 0: # the 'entrance' of the boss on the scene
            self.spriteY += 4
        if self.spriteX < 0 or self.spriteX > self.game.width - self.width:
            self.speedX *= -1
            self.speedY = self.height * 0.5
        # pick up the right sprite
        ic(self.frameX, self.frameY)
        self.image = self.boss_images[self.frameY][self.frameX]
        self.rect = self.image.get_rect()
        # check collisions boss - projectiles
        for projectile in self.game.projectiles_pool:
            if self.game.check_collisions(self, projectile) and not projectile.free and self.lives > 0:
                self.hit(1)
                projectile.reset()
        # check collisions boss - player
        
        # boss destroyed
        if self.lives < 1 and self.game.sprite_update and self.frameX < self.max_frame - 1:
            if self.frameX < self.max_frame - 1:
                self.kill()
                self.game.score += self.max_lives
            self.frameX += 1
        # calculate new coords
        self.spriteX += self.speedX
        self.spriteY += self.speedY
        # assign coords
        self.rect.x = round(self.spriteX)
        self.rect.y = round(self.spriteY)
        # draw lives
        if self.lives > 0:
            self.game.draw_text(str(self.lives), 30, self.spriteX, self.spriteY, WHITE)

class Wave:
    def __init__(self, game) -> None:
        self.game = game
        self.width = self.game.columns * self.game.enemy_size
        self.height = self.game.rows * self.game.enemy_size
        self.spriteX = self.game.width * 0.5 - self.width * 0.5
        self.spriteY = -self.height
        self.speedX = -1 if random.random() < 0.5 else 1
        self.speedY = 0
        self.enemies = []
        self.new_wave_trigger = False
        self.marked_for_deletion = False
        self.create()

    def create(self):
        for wave_y in range(0, self.game.rows):
            for wave_x in range(0, self.game.columns):
                if random.random() < 0.2:
                    self.enemies.append(Rhinomorph(self.game, wave_x * self.game.enemy_size, wave_y * self.game.enemy_size, [self.game.enemies_sprites]))
                else:
                    self.enemies.append(Beetlemorph(self.game, wave_x * self.game.enemy_size, wave_y * self.game.enemy_size, [self.game.enemies_sprites]))

    def filter_elements(self, element):
        return element

    def update(self):
        # pygame.draw.rect(self.game.screen, 'white', (self.spriteX, self.spriteY, self.width,self.height), 3)
        if self.spriteY < 0:
            self.spriteY += 5
        self.speedY = 0
        if self.spriteX < 0 or self.spriteX > self.game.width - self.width:
            self.speedX *= -1
            self.speedY = self.game.enemy_size
        self.spriteX += self.speedX
        self.spriteY += self.speedY

        for index_enemy, single_enemy in enumerate(self.enemies):
            single_enemy.update(self.spriteX, self.spriteY)
            if single_enemy.marked_for_deletion:
                self.enemies.pop(index_enemy)
                single_enemy.kill()
        # self.enemies = list(filter(lambda enemy: enemy.marked_for_deletion == 0, self.enemies)) with lambda it doesn't work

        if len(self.enemies) <= 0:
            self.marked_for_deletion = True

class Game:
    def __init__(self):
        # general setup
        pygame.init()
        pygame.display.set_caption('Space Invaders')
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))

        self.clock = pygame.time.Clock()
        self.width = WIDTH
        self.height = HEIGHT
        self.debug = True
        self.score = 0
        self.winning_score = 5
        self.game_over = False
        self.keys = set()

        self.background = pygame.image.load('./images/background.png')

        # create sprite groups
        self.all_sprites = pygame.sprite.Group()
        self.enemies_sprites = pygame.sprite.Group()
        self.bosses_sprites = pygame.sprite.Group()
        self.player_sprites = pygame.sprite.Group()
        self.projectile_sprites = pygame.sprite.Group()

        self.projectiles_pool = []
        self.number_of_projectiles = 15
        self.create_projectiles()
        self.fired = False

        self.score = 0
        self.columns = 2
        self.rows = 2
        self.enemy_size = 80

        self.sprite_update = False
        self.sprite_timer = 0
        self.sprite_interval = 150

        # sound
        # self.first_sound = pygame.mixer.Sound('../sounds/first_sound.mp3')
        # self.first_sound.set_volume(0.5)
        # self.second_sound = pygame.mixer.Sound('../sounds/second_sound.mp3')
        # self.second_sound.set_volume(0.5)

        # player
        self.player = Player(self, [self.all_sprites, self.player_sprites])

        self.restart()

    def draw_text(self, text, size, x, y, color):
        """ draw text to screen """
        self.game_font = pygame.font.Font('./fonts/impact.ttf', size)
        text_surface = self.game_font.render(str(text), True, color)
        text_rect = text_surface.get_rect(topleft = (x,y))
        self.screen.blit(text_surface, text_rect)

    def create_projectiles(self):
        for single_projectile in range(0, self.number_of_projectiles):
            self.projectiles_pool.append(Projectile([self.all_sprites, self.projectile_sprites]))

    def get_free_projectile(self):
        for single_projectile in self.projectiles_pool:
            if single_projectile.free:
                return single_projectile

    def check_collisions(self, a, b):
        """ collision detection between 2 rectangles
        only if all checks are true we have a collision """
        return (a.spriteX < b.spriteX + b.width and
                a.spriteX + a.width > b.spriteX and
                a.spriteY < b.spriteY + b.height and
                a.spriteY + a.height > b.spriteY)

    def new_wave(self):
        if randint(1, 10) < 5 and self.columns * self.enemy_size < self.width * 0.8:
            self.columns += 1
        elif self.rows * self.enemy_size < self.height * 0.6:
            self.rows += 1
        self.waves.append(Wave(self))
        for index_wave, wave in enumerate(self.waves):
            if wave.marked_for_deletion:
                self.waves.pop(index_wave)
        print(f'Waves list: {self.waves}')

    def restart(self):
        self.player.restart()
        self.columns = 2
        self.rows = 2
        self.waves = []
        self.bosses = []
        # self.waves.append(Wave(self))
        self.bosses.append(Boss(self, [self.all_sprites, self.bosses_sprites]))
        self.wave_count = 1
        self.score = 0
        self.game_over = False

    def run(self):
        # Main loop
        running = True
        get_ticks_last_frame = 0

        while running:
            ticks = pygame.time.get_ticks()
            dt = ticks - get_ticks_last_frame
            get_ticks_last_frame = ticks

            # sprite timing logic
            if self.sprite_timer > self.sprite_interval:
                self.sprite_update = True
                self.sprite_timer = 0
            else:
                self.sprite_update = False
                self.sprite_timer += dt

            # Event handling (keyboard, mouse, etc.)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_d:
                        self.debug = not self.debug
                        print(f'Debug: {self.debug}')
                    elif event.key == pygame.K_r and self.game_over:
                        self.restart()
                    elif event.key == pygame.K_1 and not self.fired:
                        self.player.shoot()
                    elif event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                    self.fired = True
                    self.keys.add(event.key)
                elif event.type == pygame.KEYUP:
                    self.fired = False
                    if event.key in self.keys:
                        self.keys.discard(event.key)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.mouse_x, self.mouse_y = event.pos
                    self.mouse_pressed = True
                    print(f'Mouse click pos: {self.mouse_x}, {self.mouse_y}')

            self.screen.fill((BLACK))

            self.screen.blit(self.background, (0, 0))

            self.all_sprites.update(dt)

            for boss in self.bosses:
                boss.update(dt)

            for wave in self.waves:
                wave.update()
                if len(wave.enemies) < 1 and not wave.new_wave_trigger and not self.game_over:
                    self.new_wave()
                    self.wave_count += 1
                    wave.new_wave_trigger = True
                    if self.player.lives < self.player.max_lives:
                        self.player.lives += 1

            self.all_sprites.draw(self.screen)
            self.enemies_sprites.draw(self.screen)

            self.draw_text(f'Score: {self.score}', 30, 20, 40, WHITE)
            self.draw_text(f'Wave: {self.wave_count}', 30, 20, 80, WHITE)
            self.draw_text(f'Lives: {self.player.lives}', 30, 20, 120, WHITE)
            self.draw_text(f'Sprite update: {self.sprite_update}', 20, 20, 160, WHITE)
            self.draw_text(f'dt: {dt}', 20, 20, 200, WHITE)

            if self.debug:
                pass
                # self.screen.blit(self.player.player_surface, (self.player.spriteX, self.player.spriteY + 75))
                # for single_enemy in self.enemies_sprites:
                #     self.screen.blit(single_enemy.enemy_surface, (single_enemy.spriteX, single_enemy.spriteY))
                # self.draw_text(f'Lost: {self.lost_hatchlings}', 40, 95, 90, WHITE)
                # self.draw_text(f'Particles: {len(self.particles_sprites)}', 40, 95, 130, WHITE)

            # win or lose messages
            if self.game_over:
                self.draw_text('GAME OVER', 100, 30, self.height // 2, WHITE)
                self.draw_text('Press R to restart', 20, 30, self.height // 2 + 30, WHITE)

            pygame.display.update()

        pygame.quit()

if __name__ == '__main__':
    game = Game() # create the instance of the game to run the main loop
    game.run() # run our game inside the main loop
